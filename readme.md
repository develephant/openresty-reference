# OpenResty Reference

__Embed the power of Lua into Nginx HTTP Servers.__

Directive and API reference for version `<=1.9.15.1`

All texts Copyright (C) Yichun Zhang (agentzh)

[![Documentation Status](https://readthedocs.org/projects/openresty-reference/badge/?version=latest)](http://openresty-reference.readthedocs.io/en/latest/?badge=latest)

__[http://openresty-reference.readthedocs.io/](http://openresty-reference.readthedocs.io/)__

_Reproduced from https://github.com/openresty/lua-nginx-module/blob/master/README.markdown_

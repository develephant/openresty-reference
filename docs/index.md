# OpenResty Reference (1.11.2.2) 

__Embed the power of Lua into Nginx HTTP Servers.__

Original texts are copyright (C) Yichun Zhang (agentzh)

### [https://openresty.org](https://openresty.org)

_This is simply a copy of the readme found [here](https://github.com/openresty/lua-nginx-module/blob/master/README.markdown). It has been split to provide an alternative way to browse the contents._
